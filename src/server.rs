use std::net::SocketAddr;
use tokio::io::{AsyncBufReadExt, AsyncWriteExt};
use tokio::net::{TcpListener, TcpStream};
use tokio::{
    io::BufReader,
    spawn,
    sync::broadcast::{self, Sender},
};

async fn handle_connection(
    mut socket: TcpStream,
    addr: SocketAddr,
    tx: Sender<(String, SocketAddr)>,
) {
    println!("Got Connection {}", &addr.to_string());
    let (reader, mut writer) = socket.split();
    let mut rx = tx.subscribe();
    let mut reader = BufReader::new(reader);
    let mut lines = String::new();
    loop {
        tokio::select! {
            _ = reader.read_line(&mut lines) =>{
                tx.send((lines.clone(),addr)).ok();
                lines.clear();
            }
            message = rx.recv() =>{
                let (message,other_addr) = match message{
                    Ok((message,addr)) => (message,addr),
                    Err(_) => {
                        break;}
                };
                println!("{:?}",message);
                if addr != other_addr{
                    writer.write_all(message.as_bytes()).await.unwrap();
                }else{
                    let _ = writer.write_all(b"Message successfully sent").await;
                    break;
                }
            }
        };
    }
}

#[tokio::main]
async fn main() {
    let soc = "127.0.0.1:3000";
    let server = TcpListener::bind(soc)
        .await
        .expect(&format!("Unable to bind on {}", soc));
    let (tx, _) = broadcast::channel::<(String, SocketAddr)>(20);
    loop {
        let (socket, addr) = server.accept().await.unwrap();
        let tx = tx.clone();
        spawn(async move {
            handle_connection(socket, addr, tx).await;
        });
    }
}

// use std::thread;
// use std::net::{TcpListener, TcpStream, Shutdown};
// use std::io::{Read, Write};

// fn handle_client(mut stream: TcpStream) {
//     let mut data = [0 as u8; 50]; // using 50 byte buffer
//     while match stream.read(&mut data) {
//         Ok(size) => {
//             // echo everything!
//             stream.write(&data[0..size]).unwrap();
//             true
//         },
//         Err(_) => {
//             println!("An error occurred, terminating connection with {}", stream.peer_addr().unwrap());
//             stream.shutdown(Shutdown::Both).unwrap();
//             false
//         }
//     } {}
// }

// fn main() {
//     let listener = TcpListener::bind("0.0.0.0:3333").unwrap();
//     // accept connections and process them, spawning a new thread for each one
//     println!("Server listening on port 3333");
//     for stream in listener.incoming() {
//         match stream {
//             Ok(stream) => {
//                 println!("New connection: {}", stream.peer_addr().unwrap());
//                 thread::spawn(move|| {
//                     // connection succeeded
//                     handle_client(stream)
//                 });
//             }
//             Err(e) => {
//                 println!("Error: {}", e);
//                 /* connection failed */
//             }
//         }
//     }
//     // close the socket server
//     drop(listener);
// }
