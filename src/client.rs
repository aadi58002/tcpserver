use std::{
    io::{Read, Write},
    net::{Shutdown, TcpStream}
};

use tokio::{
    io::{AsyncBufReadExt, BufReader},
    signal,
};

#[tokio::main]
async fn main() {
    match TcpStream::connect("127.0.0.1:3000") {
        Ok(mut stream) => {
            let mut stdin = BufReader::new(tokio::io::stdin());
            let mut input = String::new();
            loop {
                tokio::select! {
                    _ = signal::ctrl_c() => {
                        stream.shutdown(Shutdown::Both).unwrap();
                        println!("Completed graceful shutdown");
                        break;
                    }
                    _ = stdin.read_line(&mut input) =>{
                        println!("{:?}",&input);
                        stream.write_all(input.as_bytes()).unwrap();
                        match stream.read_exact(&mut []) {
                            Ok(_) => println!("Recived confirmation"),
                            Err(_) => println!("Something Went wrong"),
                        }
                        input.clear();
                    }
                }
            }
        },
        Err(err) => {
            println!("Failed to get connection : {:?}", err);
        }
    }
    // });
    // let mut stdin = std::io::stdin().lock();
    // let mut input = String::new();
    // stdin.read_line(&mut input).unwrap();
    // input = input.trim().to_string();
}

// use std::net::{TcpStream};
// use std::io::{Read, Write};
// use std::str::from_utf8;

// fn main() {
//     match TcpStream::connect("localhost:3333") {
//         Ok(mut stream) => {
//             println!("Successfully connected to server in port 3333");

//             let msg = b"Hello!";

//             stream.write(msg).unwrap();
//             println!("Sent Hello, awaiting reply...");

//             let mut data = [0 as u8; 6]; // using 6 byte buffer
//             match stream.read_exact(&mut data) {
//                 Ok(_) => {
//                     if &data == msg {
//                         println!("Reply is ok!");
//                     } else {
//                         let text = from_utf8(&data).unwrap();
//                         println!("Unexpected reply: {}", text);
//                     }
//                 },
//                 Err(e) => {
//                     println!("Failed to receive data: {}", e);
//                 }
//             }
//         },
//         Err(e) => {
//             println!("Failed to connect: {}", e);
//         }
//     }
//     println!("Terminated.");
// }
